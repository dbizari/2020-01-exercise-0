package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class NormalGoodsTest {
    @Test
    public void testNormalItemDecreaseQualityAndSellInWhenUpdate() {
        NormalGoodieExample item;

        try {
            item = new NormalGoodieExample("normal", 4, 4, new Normal());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(3).isEqualTo(item.getQuality());
        assertThat(3).isEqualTo(item.getSellIn());
    }

    @Test
    public void testNormalItemDecreaseQualityAtDoubleRateWhenSellInExpires() {
        NormalGoodieExample item;

        try {
            item = new NormalGoodieExample("normal", -1, 4, new Normal());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(2).isEqualTo(item.getQuality());
        assertThat(-2).isEqualTo(item.getSellIn());
    }

    @Test
    public void testNormalItemQualityCannotBeNegative() {
        NormalGoodieExample item;

        try {
            item = new NormalGoodieExample("normal", -1, 1, new Normal());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(0).isEqualTo(item.getQuality());
        assertThat(-2).isEqualTo(item.getSellIn());
    }

    @Test
    public void testNormalItemQualityIsMax() {
        NormalGoodieExample item;

        try {
            item = new NormalGoodieExample("normal", 40, 50, new Normal());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(49).isEqualTo(item.getQuality());
        assertThat(39).isEqualTo(item.getSellIn());
    }
}