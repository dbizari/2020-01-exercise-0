package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class BackstagePassTest {
    @Test
    public void testBackstageDecreaseAtNormalRateWhenSellInGreaterThan10() {
        BackstagePass item;

        try {
            item = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 15, 10, new Backstage());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(11).isEqualTo(item.getQuality());
        assertThat(14).isEqualTo(item.getSellIn());
    }

    @Test
    public void testBackstageDecreaseAtDoubleRateWhenSellInLowerThan10() {
        BackstagePass item;

        try {
            item = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 8, 10, new Backstage());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(12).isEqualTo(item.getQuality());
        assertThat(7).isEqualTo(item.getSellIn());
    }

    @Test
    public void testBackstageDecreaseAtTripleRateWhenSellInLowerThan5() {
        BackstagePass item;

        try {
            item = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 3, 10, new Backstage());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(13).isEqualTo(item.getQuality());
        assertThat(2).isEqualTo(item.getSellIn());
    }

    @Test
    public void testBackstageDecreaseTo0WhenSellInLowerThan0() {
        BackstagePass item;

        try {
            item = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 0, 10, new Backstage());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(0).isEqualTo(item.getQuality());
        assertThat(-1).isEqualTo(item.getSellIn());
    }
}
