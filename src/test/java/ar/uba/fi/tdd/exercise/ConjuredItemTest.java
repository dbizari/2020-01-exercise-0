package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ConjuredItemTest {
    @Test
    public void testConjuredItemDecreaseQualityAndSellInWhenUpdate() {
        PencilConjured item;

        try {
            item = new PencilConjured("normal", 4, 4, new Conjured());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(2).isEqualTo(item.getQuality());
        assertThat(3).isEqualTo(item.getSellIn());
    }

    @Test
    public void testConjuredItemDecreaseQualityAtDoubleRateWhenSellInExpires() {
        PencilConjured item;

        try {
            item = new PencilConjured("normal", -1, 4, new Conjured());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(0).isEqualTo(item.getQuality());
        assertThat(-2).isEqualTo(item.getSellIn());
    }

    @Test
    public void testConjuredItemQualityCannotBeNegative() {
        PencilConjured item;

        try {
            item = new PencilConjured("normal", -1, 1, new Conjured());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(0).isEqualTo(item.getQuality());
        assertThat(-2).isEqualTo(item.getSellIn());
    }

    @Test
    public void testConjuredItemQualityIsMax() {
        PencilConjured item;

        try {
            item = new PencilConjured("normal", 40, 50, new Conjured());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(48).isEqualTo(item.getQuality());
        assertThat(39).isEqualTo(item.getSellIn());
    }
}
