package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	// Aged Brie tests
	@Test
	public void testAgedBrieIncresaseQualityWhenIsCloserToSellInDate() {
		List<Goods> items = new ArrayList<Goods>();
		try {
			items.add(new BackstagePass("Backstage nirvana", 3, 40, new Backstage()));
			items.add(new AgedBrie("Aged Brie", -10, 12, new AgedIncreaser()));
			items.add(new PencilConjured("Dark Pencil", 2, 10, new Conjured()));
			items.add(new Sulfuras("Sulfato de cobre", 3, 80, new Legendary()));
		} catch (Exception e) {
			assertThat(e).isNull();
			return;
		}

		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(43).isEqualTo(app.getGood("Backstage nirvana").quality);
		assertThat(2).isEqualTo(app.getGood("Backstage nirvana").sellIn);

		assertThat(14).isEqualTo(app.getGood("Aged Brie").quality);
		assertThat(-11).isEqualTo(app.getGood("Aged Brie").sellIn);

		assertThat(8).isEqualTo(app.getGood("Dark Pencil").quality);
		assertThat(1).isEqualTo(app.getGood("Dark Pencil").sellIn);

		assertThat(80).isEqualTo(app.getGood("Sulfato de cobre").quality);
		assertThat(3).isEqualTo(app.getGood("Sulfato de cobre").sellIn);
	}
}
