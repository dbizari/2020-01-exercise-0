package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SulfurasTest {
    @Test
    public void testSulfurasCannotDecreaseHisQualityAndSellIn() {
        Sulfuras item;

        try {
            item = new Sulfuras("Sulfuras, Hand of Ragnaros", 4, 80, new Legendary());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(80).isEqualTo(item.getQuality());
        assertThat(4).isEqualTo(item.getSellIn());
    }

    @Test
    public void testSulfurasCannotIncreaseHisQuality() {
        Sulfuras item;

        try {
            item = new Sulfuras("Sulfuras, Hand of Ragnaros", -3, 80, new Legendary());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(80).isEqualTo(item.getQuality());
        assertThat(-3).isEqualTo(item.getSellIn());
    }

}
