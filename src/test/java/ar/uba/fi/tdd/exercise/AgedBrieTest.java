package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AgedBrieTest {
    @Test
    public void testAgedBrieIncresaseQualityWhenIsCloserToSellInDate() {
        AgedBrie item;

        try {
            item = new AgedBrie("Aged Brie", 4, 4, new AgedIncreaser());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(5).isEqualTo(item.getQuality());
        assertThat(3).isEqualTo(item.getSellIn());
    }

    @Test
    public void testAgedBrieIncresaseQualityAtDoubleRateWhenSellInDateIsExpired() {
        AgedBrie item;

        try {
            item = new AgedBrie("Aged Brie", 0, 4, new AgedIncreaser());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(6).isEqualTo(item.getQuality());
        assertThat(-1).isEqualTo(item.getSellIn());
    }

    @Test
    public void testAgedBrieQualityCannotBeGreaterThan50() {
        AgedBrie item;

        try {
            item = new AgedBrie("Aged Brie", 0, 50, new AgedIncreaser());
        } catch (Exception e) {
            assertThat(e).isNull();
            return;
        }

        item.update();

        assertThat(50).isEqualTo(item.getQuality());
        assertThat(-1).isEqualTo(item.getSellIn());
    }
}