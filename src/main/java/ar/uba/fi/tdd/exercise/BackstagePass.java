package ar.uba.fi.tdd.exercise;

public class BackstagePass extends Goods {
    public BackstagePass(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        super(name, sellIn, quality, category);
    }
}
