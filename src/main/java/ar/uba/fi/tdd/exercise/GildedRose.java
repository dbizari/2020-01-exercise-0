
package ar.uba.fi.tdd.exercise;

import java.util.Collection;
import java.util.List;

class GildedRose {
  private List<Goods> items;

    public GildedRose(List<Goods> _items) {
        items = _items;
    }

    public void updateQuality() {
        for(Goods g: items) {
            g.update();
        }
    }

    public Goods getGood(String s) {
        for (Goods g : items) {
            if ( g.getName().equals(s) ) {
                return g;
            }
        }

        return null;
    }
}
