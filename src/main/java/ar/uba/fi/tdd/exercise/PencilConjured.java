package ar.uba.fi.tdd.exercise;

public class PencilConjured extends Goods {
    public PencilConjured(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        super(name, sellIn, quality, category);
    }
}
