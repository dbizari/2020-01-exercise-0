package ar.uba.fi.tdd.exercise;

public class Sulfuras extends Goods {
    public Sulfuras(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        super(name, sellIn, quality, category);
    }
}
