package ar.uba.fi.tdd.exercise;

public abstract class Goods {
    protected int sellIn;
    protected int quality;
    protected String name;
    protected GoodsCategory category;

    public Goods(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        if (!category.isValidQuality(quality)) {
            throw new InvalidQualityAmountException("quality must be in the correct range");
        }

        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.category = category;
    }

    public int getQuality() {
        return quality;
    }

    public String getName() {
        return name;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void applyDeltaToQuality(int delta) {
        if (this.quality + delta < 0) {
            this.quality = 0;
            return;
        }

        this.quality += delta;
    }

    public void update() {
        int qualityDelta = this.category.getDeltaForQuality(this.sellIn, this.quality);
        int sellInDelta = this.category.getDeltaForSellIn();

        this.applyDeltaToQuality(qualityDelta);
        this.sellIn += sellInDelta;
    }
}
