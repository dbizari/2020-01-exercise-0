package ar.uba.fi.tdd.exercise;

public class Legendary implements GoodsCategory {
    private final int MAX_QUALITY = 80;

    @Override
    public int getDeltaForQuality(int sellIn, int quality) {
        return 0;
    }

    @Override
    public int getDeltaForSellIn() {
        return 0;
    }

    @Override
    public boolean isValidQuality(int quality) {
        return  quality == MAX_QUALITY;
    }
}
