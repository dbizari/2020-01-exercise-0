package ar.uba.fi.tdd.exercise;

public interface GoodsCategory {
    int getDeltaForQuality(int sellIn, int quality);
    int getDeltaForSellIn();
    boolean isValidQuality(int quality);
}
