package ar.uba.fi.tdd.exercise;

public class AgedBrie extends Goods{
    public AgedBrie(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        super(name, sellIn, quality, category);
    }
}
