package ar.uba.fi.tdd.exercise;

public class InvalidQualityAmountException extends Exception {
    public InvalidQualityAmountException(String errorMessage) {
        super(errorMessage);
    }
}
