package ar.uba.fi.tdd.exercise;

public class Normal implements GoodsCategory {
    private final int MAX_QUALITY = 50;

    @Override
    public int getDeltaForQuality(int sellIn, int quality) {
        if (sellIn <= 0) {
            return -2;
        }

        return -1;
    }

    @Override
    public int getDeltaForSellIn() {
        return -1;
    }

    @Override
    public boolean isValidQuality(int quality) {
        return quality <= MAX_QUALITY && quality >= 0;
    }
}
