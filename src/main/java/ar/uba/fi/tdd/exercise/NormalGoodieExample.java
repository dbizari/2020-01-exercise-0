package ar.uba.fi.tdd.exercise;

public class NormalGoodieExample extends Goods{
    public NormalGoodieExample(String name, int sellIn, int quality, GoodsCategory category) throws InvalidQualityAmountException {
        super(name, sellIn, quality, category);
    }
}
